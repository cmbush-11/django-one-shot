from django.contrib import admin
from todos.models import ToDoList, todoitem


# Register your models here.
@admin.register(ToDoList)
class ToDoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(todoitem)
class TodoitemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")
