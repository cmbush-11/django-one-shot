from django.urls import path
from todos.views import ToDoList_list, enumerated_list, new_list

urlpatterns = [
    path("", ToDoList_list, name="ToDoList_list"),
    path("<int:id>/", enumerated_list, name="enumerated_list"),
    path("create/", new_list, name="new_list"),
]
