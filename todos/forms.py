from django.forms import ModelForm
from todos.models import ToDoList


class ToDoListForm(ModelForm):
    class Meta:
        model = ToDoList
        fields = ["name"]
