# Generated by Django 4.2.1 on 2023-06-01 00:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="todoitem",
            options={"ordering": ["due_date"]},
        ),
        migrations.AlterField(
            model_name="todoitem",
            name="list",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="tasks",
                to="todos.todolist",
            ),
        ),
    ]
