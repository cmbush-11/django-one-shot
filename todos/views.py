from django.shortcuts import render, get_object_or_404, redirect
from todos.models import ToDoList
from todos.forms import ToDoListForm


def ToDoList_list(request):
    lists = ToDoList.objects.all()
    context = {
        "ToDoList_list": lists,
    }
    return render(request, "todos/lists.html", context)


def enumerated_list(request, id):
    list = get_object_or_404(ToDoList, id=id)
    context = {
        "list_object": list,
    }
    return render(request, "todos/detail.html", context)


def new_list(request):
    if request.method == "POST":
        form = ToDoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("enumerated_list", id=list.id)
    else:
        form = ToDoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
